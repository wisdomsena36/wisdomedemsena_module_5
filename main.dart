import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyAdGWKAogWSg9za8FoCM4zObgnD7Ntidbs",
          authDomain: "todoapp-3d99b.firebaseapp.com",
          projectId: "todoapp-3d99b",
          storageBucket: "todoapp-3d99b.appspot.com",
          messagingSenderId: "927804024520",
          appId: "1:927804024520:web:776349d4d74edd41a727fd",
          measurementId: "G-ZJ85XFM0Z8"));
  runApp(const MyApp());
}

final db = FirebaseFirestore.instance;
String? courseName;
String? description;
String? programName;

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePage();
}

class _MyHomePage extends State<MyHomePage> {
  late FocusNode myFocusNode;
  late TextEditingController _courseController = TextEditingController();
  late TextEditingController _descriptionController = TextEditingController();
  late TextEditingController _programController = TextEditingController();

  @override
  void initState() {
    super.initState();

    _courseController = TextEditingController();
    _descriptionController = TextEditingController();
    _programController = TextEditingController();

    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();
    _courseController.dispose();
    _descriptionController.dispose();
    _programController.dispose();
    super.dispose();
  }

  void _editItem(DocumentSnapshot documentSnapshot) {
    _courseController.text = documentSnapshot['courseName'];
    _descriptionController.text = documentSnapshot['description'];
    _programController.text = documentSnapshot['programName'];

    showModalBottomSheet(
      context: context,
      builder: (BuildContext context) {
        return showBottomSheet(context, true, documentSnapshot);
      },
    );
  }

  void _deleteShowDialog(
      BuildContext context, DocumentSnapshot documentSnapshot) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Deleting '
            '${_courseController.text = documentSnapshot['courseName']} for '
            '${_programController.text = documentSnapshot['programName']}',
            style: const TextStyle(fontWeight: FontWeight.bold),
          ),
          content: const Text('Are you sure you want to Delete this Data?'),
          actions: [
            TextButton(
              onPressed: () {
                db.collection('courses').doc(documentSnapshot.id).delete();
                _controllerClearer();
                Navigator.of(context).pop();
              },
              child: const Text(
                'YES',
                style:
                    TextStyle(color: Colors.black, 
                    fontWeight: FontWeight.bold),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: const Text(
                'NO',
                style:
                    TextStyle(color: Colors.black, 
                    fontWeight: FontWeight.bold),
              ),
            ),
          ],
        );
      },
    );
  }

  void _deleteItem(DocumentSnapshot documentSnapshot) {
    _deleteShowDialog(context, documentSnapshot);
  }

  static var myMenuItems = <String>[
    'Edit',
    'Delete',
  ];

  void _onSelect(
      String item, BuildContext context, DocumentSnapshot documentSnapshot) {
    switch (item) {
      case 'Edit':
        _editItem(documentSnapshot);
        break;
      case 'Delete':
        _deleteItem(documentSnapshot);
        break;
    }
  }

  void _controllerClearer() {
    _courseController.text = '';
    _descriptionController.text = '';
    _programController.text = '';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            _controllerClearer();
            // When the User clicks on the button, display a BottomSheet
            showModalBottomSheet(
              context: context,
              builder: (context) {
                return showBottomSheet(context, false, null);
              },
            );
          },
          child: const Icon(Icons.add),
        ),
        appBar: AppBar(
          title: const Text('Courses Page'),
          centerTitle: true,
        ),
        body: StreamBuilder(
            // Reading Items form our Database Using the StreamBuilder widget
            stream: db.collection('courses').snapshots(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (!snapshot.hasData) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              }
              return snapshot.data?.docs.length > 0
                  ? ListView.builder(
                      itemCount: snapshot.data?.docs.length,
                      itemBuilder: (context, int index) {
                        DocumentSnapshot documentSnapshot =
                            snapshot.data.docs[index];
                        return _itemListCard(documentSnapshot);
                      })
                  : const Center(
                      child: Text(
                        'No Data Created Yet. Click on' 
                        'the Add Button to Add Data.',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    );
            }));
  }

  Widget _itemListCard(DocumentSnapshot documentSnapshot) {
    return Container(
        padding: const EdgeInsets.only(right: 28.0, left: 28.0, top: 10.0),
        //width: MediaQuery.of(context).size.width * 0.95,
        child: Card(
            elevation: 8,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(18),
            ),
            child: ListTile(
              leading: CircleAvatar(
                child: Text(
                  documentSnapshot['programName'],
                  style: const TextStyle(fontSize: 14),
                ),
              ),
              title: Text(
                documentSnapshot['courseName'],
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
              subtitle: Text(
                documentSnapshot['description'],
                style: const TextStyle(fontSize: 16),
              ),
              trailing: PopupMenuButton<String>(
                  onSelected: (item) =>
                      _onSelect(item, context, documentSnapshot),
                  itemBuilder: (BuildContext context) {
                    return myMenuItems.map((String choice) {
                      return PopupMenuItem<String>(
                        value: choice,
                        child: Text(choice),
                      );
                    }).toList();
                  }),
            )));
  }

  Widget _courseNameTextFieldForm(bool isUpdate) {
    return TextFormField(
      controller: _courseController,
      validator: (courseName) {
        if (courseName == null || courseName.isEmpty) {
          return 'Please Enter Course Name';
        }
        return null;
      },
      autofocus: true,
      decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: isUpdate ? 'Update Course Name' : 'Add Course Name',
          hintText: 'E.g. Mathematics, English, Programming, etc'),
      onChanged: (String val) {
        courseName = val;
      },
    );
  }

  Widget _descriptionTextFieldForm(bool isUpdate) {
    return TextFormField(
      controller: _descriptionController,
      validator: (courseName) {
        if (courseName == null || courseName.isEmpty) {
          return 'Please Enter Course Descrription';
        }
        return null;
      },
      decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: isUpdate ? 'Update Description' : 'Add Description',
          hintText: 'E.g.This course is for Students in I.T Level 100'),
      onChanged: (String val) {
        description = val;
      },
    );
  }

  Widget _programNameTextFieldForm(bool isUpdate) {
    return TextFormField(
      controller: _programController,
      validator: (courseName) {
        if (courseName == null || courseName.isEmpty) {
          return 'Please Enter Program Name E.g. B.Sc, B.Ed, B.BA, M.Sc';
        }
        return null;
      },
      decoration: InputDecoration(
          border: const OutlineInputBorder(),
          labelText: isUpdate ? 'Update Progrom Name' : 'Add Program Name',
          hintText: 'E.g. B.Sc, B.Ed, B.BA, M.Sc'),
      onChanged: (String val) {
        programName = val;
      },
    );
  }

  Widget _elevatedButton(bool isUpdate, DocumentSnapshot? documentSnapshot) {
    return ElevatedButton(
        onPressed: () {
          if (_formKey.currentState!.validate()) {
            if (isUpdate) {
              _firebaseUpdateButton(documentSnapshot!);
            } else {
              _firebaseAddButton();
            }
            Navigator.pop(context);
          }
        },
        child: isUpdate
            ? const Text(
                'UPDATE',
              )
            : const Text(
                'ADD',
              ));
  }

  Widget _heightSpace() {
    return const SizedBox(
      height: 10.0,
    );
  }

  void _firebaseUpdateButton(DocumentSnapshot documentSnapshot) {
    db.collection('courses').doc(documentSnapshot.id).update({
      'courseName': _courseController.text,
      'description': _descriptionController.text,
      'programName': _programController.text
    });
    _controllerClearer();
  }

  void _firebaseAddButton() {
    db.collection('courses').add({
      'courseName': courseName,
      'description': description,
      'programName': programName
    });
    _controllerClearer();
  }

  final _formKey = GlobalKey<FormState>();

  showBottomSheet(
      BuildContext context, bool isUpdate, DocumentSnapshot? documentSnapshot) {
    return Padding(
        padding: const EdgeInsets.only(right: 28.0, left: 28.0, top: 10.0),
        child: Form(
            key: _formKey,
            child: Column(children: [
              _courseNameTextFieldForm(isUpdate),
              _heightSpace(),
              _descriptionTextFieldForm(isUpdate),
              _heightSpace(),
              _programNameTextFieldForm(isUpdate),
              _heightSpace(),
              _elevatedButton(isUpdate, documentSnapshot)
            ])));
  }
}
